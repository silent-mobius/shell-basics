# Variables
```sh
NAME="John"
echo $NAME
echo "$NAME"
echo "${NAME}!"
```
---

# Shell execution
```sh
echo "I'm in $(pwd)"
echo "I'm in `pwd`"
```
they are the **Same**

---

# Parameter expansions-

## Basics

```sh
name="John"
echo ${name}
echo ${name/J/j}    #=> "john" (substitution)
echo ${name:0:2}    #=> "Jo" (slicing)
echo ${name::2}     #=> "Jo" (slicing)
echo ${name::-1}    #=> "Joh" (slicing)
echo ${name:(-1)}   #=> "n" (slicing from right)
echo ${name:(-2):1} #=> "h" (slicing from right)
echo ${food:-Cake}  #=> $food or "Cake"

length=2
echo ${name:0:length}  #=> "Jo"
```
----

# Manipulation
```sh
STR="HELLO WORLD!"
echo ${STR,}   #=> "hELLO WORLD!" (lowercase 1st letter)
echo ${STR,,}  #=> "hello world!" (all lowercase)

STR="hello world!"
echo ${STR^}   #=> "Hello world!" (uppercase 1st letter)
echo ${STR^^}  #=> "HELLO WORLD!" (all uppercase)
```

---

# Default values
```sh
${FOO:-val} 	$FOO, or val if unset (or null)
${FOO:=val} 	Set $FOO to val if unset (or null)
${FOO:+val} 	val if $FOO is set (and not null)
${FOO:?message} 	Show error message and exit if $FOO is unset (or null)
```