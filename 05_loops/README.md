# Loops

There 2 main types of loops in shell scripting: 

- Serialization loops: for
- Condition loops: while, until

---

## For
purpose of `for` loop is to go through list of items. list of items can be generated in any form, such as `ls` command, wildcard, variable and so on.
for each file `do` and `done` portion of our code will execute list of commands per request. on the last run of for loop, the variable used for iteration will remain the last value of the loop.

```sh
for i in $(ls -l|awk '{print $9}')
    do
        echo $i
    done

```
---

## For loop as an arithmetic loop

As an extension of ksh and bash we use `for` loop as in C or C based languages. we start by *initializing* variable, giving it a *condition*, and *incrementing* the *initialized* variable. As the value is incremented, the code between `do` and `done` is performed. this is structure:

```sh
for (( init; condition; increment ))
    do
        code
    done
```

Here is more precise example:

```sh
for (( i=0; i<10; i++))
    do
        echo $i
    done
```

---

## While and Until

- `while` loop runs till the coditions **is** true
```sh
while [ $i -le 10 ]
    do
        echo i ; : $(( i++ ))
    done
```


- `until` loop runs until the condition **becomes** true

```sh
until [ $i -eq 0 ]
    do
        echo i ; : $(( i-- ))
    done
```

---
# based on exit status
