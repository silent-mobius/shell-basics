# Loops Practice

## Serialization Loops

- Run for loop that counts from 0 to 100
- Run for loop that counts from 100 to 0
- Create for loop that counts amount of file/folders in folder.
- Create for loop that counts amount of file in folder and in subfolders
- in debian based system, move to `/var/cache/apt/archive` folder, and run for loop that counts all `.deb` file


## Condition Loops

- Create loop that wil count till 100
- Ceate loop that count from 100 to 0
- Create a loop that will run until the size of checked file will be bigger the 4096 bytes.
- Create a loop that will run while all files have letter D or d in them.
- Create a loop that read line by line from file `/etc/passwd` till the end of file.
- Run loop that breaks when process named git will appear.
- Run loop while user named <your user> is logged in.
- Run infite loop with sleep of 2.5 seconds in between iterations.
